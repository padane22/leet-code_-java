package Q8_atoi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

// TO
public class Solution {
    public int myAtoi(String s) {
        AutoMachine auto = new AutoMachine();
        for (int i = 0; i < s.length(); i++){
            auto.get(s.charAt(i));
        }

        return (int)auto.ans * auto.sign;
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.myAtoi("   -42"));
    }
}

class AutoMachine{
    public String state;
    public long ans;
    public int sign;

    public Map<String, String[]> table = new HashMap<>();

    public AutoMachine(){
        state = "start";
        ans = 0;
        sign = 1;
        table.put("start", new String[]{"start", "signed", "number", "end"});
        table.put("signed", new String[]{"end", "end", "number", "end"});
        table.put("number", new String[]{"end", "end", "number", "end"});
        table.put("end", new String[]{"end", "end", "end", "end"});
    }

    public void get(char c){
        state = table.get(state)[get_col(c)];

        if (state == "number"){
            ans = ans * 10 + c - '0';
            ans = sign == 1 ? Math.min(ans, Integer.MAX_VALUE) : -Math.max(ans * sign, Integer.MIN_VALUE);
        }
        else if (state == "signed"){
            if (c == '-'){
                sign = -1;
            }
        }
    }

    public int get_col(char c) {
        if (c == ' '){
            return 0;
        }
        if (c == '+' || c == '-'){
            return 1;
        }
        if (c >= '0' && c <= '9'){
            return 2;
        }
        return 3;
    }

}