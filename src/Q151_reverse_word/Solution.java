package Q151_reverse_word;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Solution {
    public String reverseWords(String s) {
        s = s.trim();
        List<String> res = new ArrayList<>();
        String res_str = "";
        String temp = "";
        boolean isInWord = true;

        for (char x : s.toCharArray()){
            if (isInWord){
                if (x == ' '){
                    isInWord = false;
                    res.add(temp);
                    temp = "";
                }
                else{
                    temp += x;
                }

            }

            else {
                if (x == ' '){
                    continue;
                }
                else {
                    isInWord = true;
                    temp += x;
                }
            }
        }
        res.add(temp);

        while (!res.isEmpty()){
            int n = res.size();
            res_str += res.remove(n - 1);
            res_str += " ";
        }

        return res_str.trim();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

    }

}
