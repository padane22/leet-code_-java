package Offer_13_movingCount;

public class Solution {
    public static void main(String[] args) {
        int m = 1;
        int n = 2;
        int k = 1;
        Solution s = new Solution();
        int res = s.movingCount(m, n, k);
        System.out.println(res);
    }

    public int movingCount(int m, int n, int k) {
        int[][] canArrive = new int[m][n];
        move(0, 0, canArrive, k);
        int res = 0;
        for (int i = 0; i < m; i++){
            for (int j = 0; j < n; j++){
                if (canArrive[i][j] == 1)   res++;
            }
        }
        return res;
    }

    public void move(int m, int n, int[][] canArrive, int k){
        if (count(m, n) <= k){
            canArrive[m][n] = 1;
            if (m > 0 && canArrive[m - 1][n] == 0)
                move(m - 1, n, canArrive, k);

            if (m < canArrive.length - 1 && canArrive[m + 1][n] == 0)
                move(m + 1, n, canArrive, k);

            if (n > 0 && canArrive[m][n - 1] == 0)
                move(m, n - 1, canArrive, k);

            if (n < canArrive[0].length - 1 && canArrive[m][n + 1] == 0)
                move(m, n + 1, canArrive, k);
        }
    }

    public int count(int m, int n){
        int sum = 0;
        while (m != 0 || n != 0){
            sum += m % 10;
            sum += n % 10;
            m /= 10;
            n /= 10;
        }
        return sum;
    }
}
