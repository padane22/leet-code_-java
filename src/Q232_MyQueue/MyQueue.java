package Q232_MyQueue;

import java.util.Stack;

public class MyQueue {
    private Stack s_in;
    private Stack s_out;

    /** Initialize your data structure here. */
    public MyQueue() {
        s_in = new Stack<Integer>();
        s_out = new Stack<Integer>();
    }

    /** Push element x to the back of queue. */
    public void push(int x) {
        s_in.push(x);
    }

    /** Removes the element from in front of queue and returns that element. */
    public int pop() {
        if(s_out.empty()){
            while(!s_in.empty()){
                s_out.push(s_in.pop());
            }
        }

        if(s_out.empty()){
            return -1;
        }
        else
            return (int)s_out.pop();
    }

    /** Get the front element. */
    public int peek() {
        int res = this.pop();
        s_out.push(res);
        return res;
    }

    /** Returns whether the queue is empty. */
    public boolean empty() {
        if(s_out.empty() && s_in.empty()){
            return true;
        }
        else
            return false;
    }

}

