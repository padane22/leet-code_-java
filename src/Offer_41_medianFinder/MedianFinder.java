package Offer_41_medianFinder;

import java.util.PriorityQueue;

public class MedianFinder {
    PriorityQueue<Integer> a, b;

    /** initialize your data structure here. */
    public MedianFinder() {
        a = new PriorityQueue<>();  // 从小到大的队列
        b = new PriorityQueue<>((x, y) -> y - x);      // 从大到小的队列
    }

    public void addNum(int num) {
        if (a.size() == b.size()){
            a.add(num);
            b.add(a.poll());
        }
        else{
            b.add(num);
            a.add(b.poll());
        }
    }

    public double findMedian() {
        return a.size() == b.size() ? ((double)a.peek() + (double)b.peek()) / 2 : (double)b.peek();
    }
}
