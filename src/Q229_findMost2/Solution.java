package Q229_findMost2;

import java.util.*;

public class Solution {
    public List<Integer> majorityElement(int[] nums) {
        List<Integer> res = new LinkedList<>();
        Map<Integer, Integer> map = new HashMap<>();
        int n = nums.length;
        for (int e : nums){
            int temp = map.getOrDefault(e, 0);
            map.put(e, temp + 1);
            if (temp == n / 3){
                res.add(e);
            }
        }
        return res;
    }
}
