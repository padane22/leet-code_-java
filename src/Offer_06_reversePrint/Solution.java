package Offer_06_reversePrint;
import Structure.*;

public class Solution {
    public int[] reversePrint(ListNode head) {
        if(head == null)
            return new int[0];
        ListNode pre = head;
        ListNode cur = head.next;
        pre.next = null;
        int flag = 1;

        while(cur != null){
            ListNode next = cur.next;

            cur.next = pre;
            pre = cur;
            cur = next;
            flag++;
        }

        ListNode new_head = pre;
        int[] res = new int[flag];

        int i = 0;
        while(new_head != null){
            res[i++] = new_head.val;
            new_head = new_head.next;
        }

        return res;

    }
}
