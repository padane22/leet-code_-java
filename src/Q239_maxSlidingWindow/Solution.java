package Q239_maxSlidingWindow;

import java.util.Deque;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class Solution {

    public int[] maxSlidingWindow(int[] nums, int k){
        if(nums.length == 0)
            return new int[0];

        Deque<Integer> q = new LinkedList<>();
        int[] res = new int[nums.length - k + 1];

        for(int j = 0, i = 0 - k + 1; j < nums.length; i++, j++){
            if (i > 0 && q.getFirst() == nums[i - 1]){
                q.removeFirst();
            }

            while(!q.isEmpty() && q.peekLast() < nums[j]){
                q.removeLast();
            }

            q.offer(nums[j]);
            if (i >= 0)
                res[i] = q.peekFirst();

        }

        return res;
    }




    /**
     * 超时暴力解法
     * @param nums
     * @param k
     * @return
     */
    public int[] maxSlidingWindow_force(int[] nums, int k) {
        int[] res = new int[nums.length - k + 1];
        int left = 0;
        int right = left + k - 1;

        while(right < nums.length){
            res[left] = nums[left];
            int i = left + 1;
            while (i <= right){
                if (nums[i] > res[left])
                    res[left] = nums[i];
                i++;
            }

            left++;
            right++;
        }

        return res;
    }
}
