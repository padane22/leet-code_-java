package Q383_ransom_letter;

import java.util.HashMap;
import java.util.Map;

public class Solution {
    public boolean canConstruct(String ransomNote, String magazine) {
        int len_mag = magazine.length();
        int len_ran = ransomNote.length();

        if (len_ran > len_mag)
            return false;

        Map<Character, Integer> magMap = new HashMap<>();
        Map<Character, Integer> ranMap = new HashMap<>();
        for (int i = 0; i < len_mag; i++){
            char char_temp = magazine.charAt(i);
            magMap.put(char_temp, magMap.getOrDefault(char_temp, 0) + 1);
        }
        for (int i = 0; i < len_ran; i++){
            char char_temp = ransomNote.charAt(i);

            if (!magMap.containsKey(char_temp))
                return false;
            else {
                ranMap.put(char_temp, ranMap.getOrDefault(char_temp, 0) + 1);
                if (ranMap.get(char_temp) > magMap.get(char_temp))
                    return false;
            }
        }

        return true;
    }
}
