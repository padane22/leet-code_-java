package Q59_matrix;

public class Solution {
    public int[][] generateMatrix(int n) {
        int[][] result = new int[n][n];
        int times = (int)Math.ceil((float)n/2); //向上取整
        int min_size = 0;
        int max_size = n - 1;
        int increNum = 1;

        for(int t = 0; t < times; t++){
            int i = min_size, j = min_size;

            for(;j <= max_size; j++){
                if(result[i][j] == 0)
                    result[i][j] = increNum++;
            }
            j = max_size;

            for(; i <= max_size; i++){
                if(result[i][j] == 0)
                    result[i][j] = increNum++;
            }
            i = max_size;

            for(; j >= min_size; j--){
                if(result[i][j] == 0)
                    result[i][j] = increNum++;
            }
            j = min_size;

            for(; i >= min_size; i--){
                if(result[i][j] == 0)
                    result[i][j] = increNum++;
            }
            min_size++;
            max_size--;

        }
        return result;
    }

    public static void main(String[] args) {
        int[][] result = new int[2][2];
        System.out.println(result[0][1]);
    }
}
