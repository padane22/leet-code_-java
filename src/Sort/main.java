package Sort;

import java.util.Arrays;

public class main {
    public static void main(String[] args) {
        int[] arr = {4, 5, 2, 3, 1, 9, 20, 100, 39, 11, 9};

        main m = new main();
        m.show(m.qsortMain(arr));
    }

    /**
     * 显示结果
     * @param arr
     */
    public void show(int[] arr){
        for(int i = 0; i < arr.length; i++){
            System.out.print(arr[i]);
            System.out.print(" ");
        }
    }

    /**
     * 交换数组中的两个元素
     * @param arr
     * @param i
     * @param j
     */
    public void swap(int[] arr, int i, int j){
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    /**
     * 冒泡排序
     */
    public int[] bubbleSort(int[] arr){

        for(int i = 0; i < arr.length; i++){
            for(int j = 0; j < arr.length - i - 1; j++){
                if(arr[j] > arr[j + 1]){
                    this.swap(arr, j, j+1);
                }
            }
        }

        return arr;
    }

    /**
     * 选择排序
     * @param arr
     * @return
     */
    public int[] selectionSort(int[] arr){
        for(int i = 0; i < arr.length - 1; i++){
            int min = arr[i];
            int minIndex = i;
            for(int j = i; j < arr.length; j++){
                if(arr[j] < min){
                    min = arr[j];
                    minIndex = j;
                }
            }
            this.swap(arr, i, minIndex);
        }
        return arr;
    }

    /**
     * 插入排序
     * @param arr
     * @return
     */
    public int[] insertSort(int[] arr){

        for(int i = 1; i < arr.length; i++){
            int temp = arr[i];
            for(int j = 0; j < i; j++){
                if(temp < arr[j]){
                    for(int k = i - 1; k >= j; k--){
                        arr[k + 1] = arr[k];
                    }
                    arr[j] = temp;
                    break;
                }
            }
        }

        return arr;
    }

    /**
     * 快速排序
     */
    public int[] qsortMain(int[] arr){
        return quickSort(arr, 0, arr.length - 1);
    }

    public int[] quickSort(int[] arr, int left, int right){
        if (left < right){
            int partitionIdx = partition(arr, left, right);

            quickSort(arr, left, partitionIdx - 1);
            quickSort(arr, partitionIdx + 1, right);
        }
        return arr;
    }

    public int partition(int[] arr, int left, int right){
        int pivot = left;
        int idx = pivot + 1;

        for (int i = idx; i <= right; i++){
            if (arr[i] < arr[pivot]){
                swap(arr, i, idx);
                idx++;
            }
        }
        swap(arr, pivot, idx - 1);

        return idx - 1;
    }

}
