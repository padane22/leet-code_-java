package Q198_rob;

public class Solution {

    /**
     * 滚动数组优化
     * @param nums
     * @return
     */
    public int rob2(int[] nums) {
        if (nums.length == 1)   return nums[0];
        int first, second;
        first = nums[0];
        second = Math.max(nums[0], nums[1]);

        for (int i = 2; i < nums.length; i++){
            int temp = second;
            second = Math.max(second, first + nums[i]);
            first = temp;
        }

        return second;
    }

    public int rob(int[] nums) {
        if (nums.length == 1)   return nums[0];
        int[] dp = new int[nums.length];
        dp[0] = nums[0];
        dp[1] = Math.max(nums[0], nums[1]);

        for (int i = 2; i < nums.length; i++){
            dp[i] = Math.max(dp[i - 1], dp[i - 2] + nums[i]);
        }

        return dp[nums.length - 1];
    }
}
