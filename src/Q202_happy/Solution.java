package Q202_happy;

import java.util.HashSet;
import java.util.Set;

public class Solution {

    public boolean isHappy(int n) {
        Set<Integer> res = new HashSet<>();
        res.add(n);
        while (true) {
            String str_n = Integer.toString(n);
            int len_n = str_n.length();
            int sum = 0;
            for (int i = 0; i < len_n; i++) {
                sum += (int) Math.pow(Character.getNumericValue(str_n.charAt(i)), 2);
            }

            n = sum;
            if (n == 1)
                return true;

            if (res.contains(n)) {
                return false;
            } else {
                res.add(n);
            }
        }

    }

    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.isHappy(19));
    }
}
