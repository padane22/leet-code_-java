package Q19;
import Structure.*;

public class Solution {
    // test
    public ListNode removeNthFromEnd(ListNode head, int n) {
        int size = 0;
        ListNode cur = head;
        while (cur != null){    // 求出链表长度
            cur = cur.next;
            size++;
        }

        int index = size - n;   // 求出倒数第n个的索引

        // 将其移除
        if (index == 0)
            head = head.next;
        else {
            ListNode pre = null;
            cur = head;
            for(int i = 1; i <= index; i++){
                pre = cur;
                cur = cur.next;
            }
            pre.next = cur.next;
        }
        return head;
    }
}
