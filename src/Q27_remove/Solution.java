package Q27_remove;

public class Solution {
    public int remove(int[] nums, int val){
        int n = nums.length;
        int tail = n - 1;

        for(int i = 0; i < n; i++){
            int cur = nums[i];
            if(cur == val){
                int temp = nums[tail];
                nums[tail] = val;
                nums[i] = temp;
                n--;
                i--;
                tail--;
            }

        }
        return n;
    }

    public static void main(String[] args){
        int[] nums = {3,2,2,3};

        Solution s = new Solution();
        System.out.println(s.remove(nums, 3));
    }
}
