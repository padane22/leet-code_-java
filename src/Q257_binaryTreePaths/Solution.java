package Q257_binaryTreePaths;
import Structure.*;

import java.util.ArrayList;
import java.util.List;

public class Solution {
    public List<String> binaryTreePaths(TreeNode root) {
        List<String> res = new ArrayList<>();
        dfs(root, "", res);
        return res;
    }

    public void dfs(TreeNode root, String path, List<String> paths){
        if (root != null){
            path += Integer.toString(root.val);

            if (root.left == null && root.right == null){
                paths.add(path);
            }
            else{
                path += "->";
                dfs(root.left, path, paths);
                dfs(root.right, path, paths);
            }
        }
    }
}
