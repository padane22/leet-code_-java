package Q225_mystack;

import java.util.LinkedList;
import java.util.Queue;

public class MyStack {
    private Queue<Integer> q1;
    private Queue<Integer> q2;
    private int len = 0;

    /** Initialize your data structure here. */
    public MyStack() {
        q1 = new LinkedList<Integer>();
        q2 = new LinkedList<Integer>();
    }

    /** Push element x onto stack. */
    public void push(int x) {
        q1.offer(x);
        len++;
    }

    /** Removes the element on top of the stack and returns that element. */
    public int pop() {
        for (int i = 0; i < len - 1; i++){
            q2.add(q1.poll());
        }
        int res = q1.poll();
        q1 = q2;
        q2 = new LinkedList<Integer>();
        len--;
        return res;
    }

    /** Get the top element. */
    public int top() {
        int res = this.pop();
        this.push(res);
        return res;
    }

    /** Returns whether the stack is empty. */
    public boolean empty() {
        return len == 0;
    }

    public static void main(String[] args) {
        MyStack obj = new MyStack();
        obj.push(1);
        obj.push(2);
        int param_2 = obj.pop();
        int param_3 = obj.top();
        boolean param_4 = obj.empty();

        System.out.println(param_2);
        System.out.println(param_3);
        System.out.println(param_4);
    }
}
