package Q28_strStr_KMP;

public class Solution {
    public int strStr(String haystack, String needle) {
        int h_len = haystack.length();
        int n_len = needle.length();

        if(n_len == 0)
            return 0;

        for (int i = 0; i <= h_len - n_len; i++){
            int flag = 0;
            for(int j = 0; j < n_len; j++){
                if (needle.charAt(j) == haystack.charAt(i + j))
                    flag++;

                if(flag == n_len)
                    return i;
            }
        }

        return -1;
    }

    public int strStrKMP(String haystack, String needle) {
        int[] next = new int[needle.length()];
        getNext(next, needle);

        int i = 0, j = 0;

        while(i < haystack.length() && j < needle.length()){
            if (haystack.charAt(i) == needle.charAt(j)){
                i++;
                j++;
            }
            else j = next[j] + 1;
        }

        if (j >= needle.length())
            return i - needle.length();
        else
            return  -1;
    }

    public void getNext(int[] next, String s){
        next[0] = -1;
        int j = -1, i = 1;
        for (; i < s.length(); i++){
            while (j >= 0 && s.charAt(i) != s.charAt(j + 1)){
                j = next[j];
            }
            if(s.charAt(i) == s.charAt(j + 1)){
                j++;
            }
            next[i] = j;
        }
    }

    public static void main(String[] args) {
        Solution s = new Solution();

        System.out.println(s.strStrKMP("aabaabaafa", "aabaaf"));
    }
}
