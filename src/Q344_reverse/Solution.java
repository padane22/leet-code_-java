package Q344_reverse;

public class Solution {
    public void reverseString(char[] s) {
        int s_len = s.length;
        int left = 0;
        int right = s_len - 1;

        while (left < right){
            char temp = s[left];
            s[left] = s[right];
            s[right] = temp;

            left++;
            right--;
        }

        return;
    }

    public void reverseString2(char[] s) {

    }
}
