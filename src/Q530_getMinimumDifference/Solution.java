package Q530_getMinimumDifference;
import Structure.*;

public class Solution {
    int res = Integer.MAX_VALUE;
    public int getMinimumDifference(TreeNode root) {
        int dis1 = Integer.MAX_VALUE;
        int dis2 = Integer.MAX_VALUE;
        if (root.left != null)
            dis1 = Math.abs(root.val - findRightValue(root.left));
        if (root.right != null)
            dis2 = Math.abs(root.val - findLeftValue(root.right));
        int min = dis1 < dis2 ? dis1 : dis2;
        res = res < min ? res : min;
        if (root.left != null)
            getMinimumDifference(root.left);
        if (root.right != null)
            getMinimumDifference(root.right);
        return res;
    }
    public int findLeftValue(TreeNode root){
        while (root.left != null){
            root = root.left;
        }
        return root.val;
    }
    public int findRightValue(TreeNode root){
        while (root.right != null){
            root = root.right;
        }
        return root.val;
    }
}
