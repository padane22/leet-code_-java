package Q203_remove;

import Structure.*;


public class Solution {
    public ListNode removeElements(ListNode head, int val) {
        ListNode pre = null;
        ListNode cur = head;

        while(cur != null){
            if(cur.val == val){
                if(pre != null){
                    pre.next = cur.next;
                }
                else {
                    head = head.next;
                }

            }
            else {
                pre = cur;
            }
            cur = cur.next;
        }
        return head;
    }

    public static void main(String[] args) {
        ListNode pre = new ListNode();
        System.out.println(pre == null);
    }

}


