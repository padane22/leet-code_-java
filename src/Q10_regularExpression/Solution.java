package Q10_regularExpression;

public class Solution {

    public static void main(String[] args) {
        Solution s = new Solution();
        s.isMatch("aaaaaa", "aaaaaa");
    }

    public boolean isMatch(String s, String p) {
        int s_len = s.length();
        int p_len = p.length();

        boolean[][] f = new boolean[s_len + 1][p_len + 1];
        f[0][0] = true;

        /**
         * 循环中的 i, j 指的是字符串中第 i, j 个字符，其对应的下标依次是 i - 1, j - 1
         */
        for (int i = 0; i <= s_len; i++){
            for (int j = 1; j <= p_len; j++){
                // to understand
                if (p.charAt(j - 1) == '*'){
                    f[i][j] = f[i][j - 2];
                    if (matches(s, p, i, j - 1)){
                        f[i][j] = f[i - 1][j] || f[i][j];
                    }
                }
                else{
                    if (matches(s, p, i, j)){
                        f[i][j] = f[i - 1][j - 1];
                    }
                }
            }
        }
        this.showF(f);
        return f[s_len][p_len];
    }

    public boolean matches(String s, String p, int i, int j){
        if (i == 0){
            return false;
        }

        if (p.charAt(j - 1) == '.')
            return true;
        else
            return s.charAt(i - 1) == p.charAt(j - 1);
    }

    public void showF(boolean[][] f){
        int m = f.length;
        int n = f[0].length;

        for (int i = 0; i < m; i++){
            for (int j = 0; j < n; j++){
                System.out.print(f[i][j]);
                System.out.print(" ");
            }
            System.out.println("");
        }
    }
}
