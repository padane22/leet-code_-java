package Offer_12_exist;

public class Solution {
    public boolean exist(char[][] board, String word) {
        int m = board.length;
        int n = board[0].length;

        char[] words = word.toCharArray();

        if (m * n < word.length())  return false;

        for (int i = 0; i < m; i++){
            for (int j = 0; j < n; j++){
                return dfs(board, words, i, j, 0);
            }
        }
        return false;

    }

    public boolean dfs(char[][] board, char[] words, int i, int j, int k){
        boolean res = false;
        if (i < 0 || i >= board.length || j < 0 || j >= board[0].length || k >= words.length || board[i][j] != words[k]) return false;

        board[i][j] = '\0';
        if (k == words.length - 1)  return true;

        res = dfs(board, words, i - 1, j, k + 1) || dfs(board, words, i + 1, j, k + 1)
        || dfs(board, words, i, j - 1, k + 1) || dfs(board, words, i, j + 1, k + 1);

        board[i][j] = words[k];
        return res;
    }
}
