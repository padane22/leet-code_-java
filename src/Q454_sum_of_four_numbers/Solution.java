package Q454_sum_of_four_numbers;

import java.util.HashMap;
import java.util.Map;

public class Solution {
    public int fourSumCount(int[] nums1, int[] nums2, int[] nums3, int[] nums4) {
        Map<Integer, Integer> countAB = new HashMap<Integer, Integer>();
        for (int i : nums1){
            for (int j : nums2){
                int sum_ij = i + j;     // 增加执行用时，也增加内存消耗；原因不明
                countAB.put(sum_ij, countAB.getOrDefault(sum_ij, 0) + 1);
            }
        }

        int res = 0;

        for (int i : nums3) {
            for (int j : nums4) {
                int sum_ij = i + j;     // 增加执行用时，也增加内存消耗
                if (countAB.containsKey(-sum_ij)){
                    res += countAB.get(-sum_ij);
                }
            }
        }

        return res;
    }
}
