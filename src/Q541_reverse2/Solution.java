package Q541_reverse2;

public class Solution {
    public String reverseStr(String s, int k) {
        char[] s_char = s.toCharArray();
        int begin = 0;

        while (begin < s.length() - 1){
            int left = begin;
            int right = left + k - 1;

            if (right > s.length() - 1)
                right = s.length() - 1;

            while (left < right){
                char temp = s_char[left];
                s_char[left] = s_char[right];
                s_char[right] = temp;

                left++;
                right--;
            }

            begin += 2 * k;
        }

        String res = String.valueOf(s_char);
        return res;
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.reverseStr("abcdefg", 2));
    }
}
