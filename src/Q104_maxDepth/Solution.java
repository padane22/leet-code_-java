package Q104_maxDepth;
import Structure.*;

import java.util.Map;

public class Solution {

    public int maxDepth(TreeNode root) {
        if (root == null)
            return 0;
        int n_left = maxDepth(root.left) + 1;
        int n_right = maxDepth(root.right) + 1;

        return Math.max(n_left, n_right);
    }

    public int maxDepth2(TreeNode root) {
        if (root == null)
            return 0;
        int n_left = maxDepth(root.left);
        int n_right = maxDepth(root.right);

        return Math.max(n_left, n_right) + 1;
    }
}
