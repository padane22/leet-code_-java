package Q142;
import Structure.*;

import java.util.HashSet;
import java.util.Set;

public class Solution {
    public ListNode detectCycle(ListNode head) {
        ListNode cur = head;
        Set<ListNode> visited = new HashSet<ListNode>();

        while (cur != null) {
            if (visited.contains(cur))
                return cur;
            visited.add(cur);
            cur = cur.next;
        }

        return null;
    }
}
