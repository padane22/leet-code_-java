package Q110_isBalanced;
import Structure.*;

public class Solution {
    public boolean isBalanced(TreeNode root) {
        if(root == null)
            return true;

        return Math.abs(height(root.left) - height(root.right)) < 2 && isBalanced(root.left) && isBalanced(root.right);
    }

    public int height(TreeNode t){
        if (t == null)
            return 0;
        int n_left = height(t.left);
        int n_right = height(t.right);

        return Math.max(n_left, n_right) + 1;
    }


}
