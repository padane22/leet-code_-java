package Q113_pathSum2;

import Structure.*;
import java.util.LinkedList;
import java.util.List;

public class Solution {
    List<List<Integer>> res = new LinkedList<>();
    List<Integer> l = new LinkedList<Integer>();

    public List<List<Integer>> pathSum(TreeNode root, int targetSum) {
        dfs(root, targetSum, 0);
        return res;
    }

    public void dfs(TreeNode root, int targetSum, int sum){
        if (root != null){
            sum += root.val;
            l.add(root.val);
        }
        else return;

        if (root.left == null && root.right == null){
            if (sum == targetSum){
                res.add(new LinkedList<Integer>(l));
            }
            else{
                l.remove(l.size() - 1);     // 回溯
                return;
            }   // 此处 else 可删去
        }

        dfs(root.left, targetSum, sum);
        dfs(root.right, targetSum, sum);
        l.remove(l.size() - 1);             // 回溯
    }
}
