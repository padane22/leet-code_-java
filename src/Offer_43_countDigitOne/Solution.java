package Offer_43_countDigitOne;

import java.util.Collections;

public class Solution {
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.countDigitOne(824883294));
    }

    public int countDigitOne(int n){

        int n_len = Integer.toString(n).length();
        int [][] dp = new int[n_len + 1][10];
        dp = getdp(dp);

        return 0;
    }

    public int[][] getdp(int[][] dp){
        for (int i = 0; i <= dp.length; i++){

        }

        return dp;
    }

    /**
     * 差不多也是暴力解法
     * @param n
     * @return
     */
    public long countDigitOne_force2(long n){
        long pre = 0;
        for (long i = 0; i <= n; i++){
            long num = 0;
            char[] c = Long.toString(i).toCharArray();
            for (char e : c){
                num++;
            }
            pre += num;
        }
        return pre;
    }


    /**
     * 暴力解法 超时
     * @param n
     * @return
     */
    public int countDigitOne_force(int n){
        int count = 0;
        for (int i = 0; i <= n; i++){
            String s = Integer.toString(i);
            char[] sc = s.toCharArray();
            for (char e : sc){
                if(e == '1')
                    count++;
            }
        }
        return count;
    }
}
