package Offer_09_CQueue;

import java.util.Stack;

public class CQueue {
    private Stack s_in;
    private Stack s_out;

    public CQueue() {
        s_in = new Stack();
        s_out = new Stack();
    }

    public void appendTail(int value) {
        s_in.push(value);
    }

    public int deleteHead() {

        // 如果为空，做一个搬移
        if (s_out.empty()) {
            while (!s_in.empty()) {
                s_out.push(s_in.pop());
            }
        }

        // 如果还是为空
        if (s_out.empty())
            return -1;
        else {
            return (int)s_out.pop();
        }

    }

}
