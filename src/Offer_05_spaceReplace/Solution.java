package Offer_05_spaceReplace;

public class Solution {
    public String replaceSpace(String s) {
        int s_len = s.length();
        String res = "";

        int begin = 0;

        for (int i = 0; i < s_len; i++){
            if (s.charAt(i) == ' '){
                res += s.substring(begin, i);
                res += "%20";
                begin = i + 1;
            }
        }

        res += s.substring(begin, s_len);
        return res;
    }
}
