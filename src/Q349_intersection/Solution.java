package Q349_intersection;

import java.lang.reflect.Array;
import java.util.*;

public class Solution {
    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> set_nums1 = new HashSet<>();
        int l_n1 = nums1.length;
        int l_n2 = nums2.length;
        List<Integer> res = new ArrayList<>();

        for (int i = 0; i < l_n1; i++){
            set_nums1.add(nums1[i]);
        }

        for (int i = 0; i < l_n2; i++){
            int temp = nums2[i];
            if (set_nums1.contains(temp) && !res.contains(temp)){
                res.add(temp);
            }
        }

        int l_res = res.size();
        int[] res_array = new int[l_res];
        int i = 0;
        while (!res.isEmpty()){
            res_array[i] = res.remove(0);
            i++;
        }

        return res_array;
    }
}
