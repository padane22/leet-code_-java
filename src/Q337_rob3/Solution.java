package Q337_rob3;
import Structure.*;

import java.util.LinkedList;
import java.util.Queue;

public class Solution {
    public int rob(TreeNode root) {
        int pre = 0, cur = 0;
        boolean preIsRob = false;
        Queue<TreeNode> q_temp = new LinkedList<>(), q_cur = new LinkedList<>();

        q_cur.offer(root);

        while (!q_cur.isEmpty() && !q_temp.isEmpty()){
            int sum = 0;
            while(!q_cur.isEmpty()){
                TreeNode temp = q_cur.poll();
                if (temp == null)   continue;
                q_temp.offer(temp);
                sum += temp.val;
            }

            if (preIsRob){
                if (pre + sum > cur){
                    int temp = cur;
                    cur = pre + sum;
                    pre = temp;
                    preIsRob = true;
                }
                else{
                    preIsRob = false;
                }
            }
            else {
                int temp = cur;
                cur += sum;
                pre = temp;
                preIsRob = true;
            }

            while(!q_temp.isEmpty()){
                TreeNode temp = q_temp.poll();
                if (temp == null)   continue;
                q_cur.offer(temp.left);
                q_cur.offer(temp.right);
            }
        }
        return cur;
    }
}
