package Offer_58_reverseLeftWords;

public class Solution {
    public String reverseLeftWords(String s, int n) {
        int s_len = s.length();
        if (s_len == n)
            return s;

        String p1 = s.substring(0, n);
        String p2 = s.substring(n);

        return p2 + p1;

    }
}
