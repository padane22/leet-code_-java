package Q209_array;

public class Solution {
    public int find(int[] nums, int target){
        int n = nums.length;
        int l = Integer.MAX_VALUE;

        for(int i = 0; i < n; i++){
            int sum = 0;
            for (int j = i; j < n; j++){
                sum += nums[j];
                if(sum >= target){
                    int subl = j-i+1;
                    l = l < subl ? l : subl;
                }
            }

        }
        return l == Integer.MAX_VALUE ? 0 : l;

    }

    public int find2(int[] nums, int target){
        int n = nums.length;
        int left = 0, right = 0;
        int l = Integer.MAX_VALUE;
        while(right < n){
            int sum = 0;
            for(int i = left; i <= right; i++) {
                sum += nums[i];
            }
            if(sum >= target){
                int curl = right - left + 1;
                l = l < curl ? l :curl;
                left++;
            }
            else{
                right++;
            }
        }
        return l == Integer.MAX_VALUE ? 0 : l;
    }

    public static void main(String[] args) {
        int[] nums = {2,3,1,2,4,3};
        Solution s = new Solution();
        System.out.println(s.find(nums, 7));
    }
}
