package Q322_coinChange;

import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;

public class Solution {

    public int coinChange(int[] coins, int amount) {
        int[] dp = new int[amount + 1];
        Arrays.fill(dp, amount + 1);
        dp[0] = 0;

        for (int i = 1; i <= amount; i++){
            for (int c : coins){
                if (c <= amount){
                    dp[i] = Math.min(dp[i], dp[i - c] + 1);
                }
            }
        }

        return dp[amount] >= amount + 1 ? -1 : dp[amount];
    }


    /**
     * 方法超时
     * @param coins
     * @param amount
     * @return
     */
    public int coinChange2(int[] coins, int amount) {
        if (amount == 0)
            return 0;
        int[] res = new int[coins.length];
        for (int i = 0; i < coins.length; i++){
            if (amount >= coins[i])
                res[i] = coinChange2(coins, amount - coins[i]) + 1;
            else{
                res[i] = Integer.MIN_VALUE;
            }
        }

        int min = Integer.MAX_VALUE;
        int sum = 0;

        for (int i : res){
            if (i > 0){
                sum += i;
                if (i < min)
                    min = i;
            }
        }

        return sum == 0 ? -1 : min;
    }
}
