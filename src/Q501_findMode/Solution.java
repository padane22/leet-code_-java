package Q501_findMode;

import Structure.TreeNode;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Solution {
//    List<Integer> res = new LinkedList<>();
    int res = 0;
    int c = 0;
    public void findMode(TreeNode root) {
        help(root, 0);
        return;
    }
    public void help(TreeNode root, int count){
        if (root.left != null){
            if (root.val == root.left.val){
                help(root.left, count + 1);
            }
            else{
                if (count > c){
                    c = count;
                    res = root.val;
                }
                help(root.left, 0);
            }
        }
        if (root.right != null){
            if (root.val == root.right.val){
                help(root.right, count + 1);
            }
            else {
                if (count > c) {
                    c = count;
                    res = root.val;
                }
                help (root.right, 0);
            }
        }
    }
}


