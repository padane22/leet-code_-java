package Q1343_numOfSubarrays;

public class Solution {
    public int numOfSubarrays(int[] arr, int k, int threshold) {
        int res = 0;
        int sum = 0;
        for (int i = 0; i < k; i++){
            sum += arr[i];
        }
        for (int i = 0; i < arr.length - k + 1; i++){
            if (i != 0){
                sum -= arr[i - 1];
                sum += arr[i + k - 1];
            }
            if (sum >= k * threshold)   res++;      // 除法变乘法，妙！
        }

        return res;
    }
}
