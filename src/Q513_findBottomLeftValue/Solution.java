package Q513_findBottomLeftValue;
import Structure.*;

import java.util.LinkedList;
import java.util.Queue;

public class Solution {
    /**
     * 递归方法中的参数
     * 用全局来存放目前已遍历到的最深层的第一个值
     */
    public int res = 0;
    public int resLayer = -1;

    /**
     * 递归方法
     * @param root
     * @return
     */
    public int findBottomLeftValue(TreeNode root) {
        dfs(root, 0);
        return res;
    }
    public void dfs(TreeNode root, int layer){
        if (root == null){
            return;
        }

        if (root.left == null && root.right == null){
            if (layer > resLayer){
                res = root.val;
                resLayer = layer;
            }
        }

        dfs(root.left, layer + 1);
        dfs(root.right, layer + 1);

    }

    /**
     * 层序遍历
     * @param root
     * @return
     */
    public int findBottomLeftValue2(TreeNode root){
        Queue<TreeNode> que = new LinkedList<>();
        int res = 0;
        que.offer(root);

        while (!que.isEmpty()){
            int size = que.size();
            for (int i = 0; i < size; i++){
                TreeNode temp = que.poll();
                if (i == 0)
                    res = temp.val;
                if (temp.left != null)
                    que.offer(temp.left);
                if (temp.right != null)
                    que.offer(temp.right);
            }
        }

        return res;
    }
}
