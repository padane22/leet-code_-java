package Q242_letter_heterotopic_words;

import java.util.HashSet;
import java.util.Set;

public class Solution {
    public boolean isAnagram(String s, String t) {
        int s_len = s.length();
        int t_len = t.length();
        int[] cs = new int[26];

        for (int i = 0; i < s_len; i++){
            cs[s.charAt(i) - 'a']++;
        }

        for (int i = 0; i < t_len; i++){
            cs[t.charAt(i) - 'a']--;
        }

        for (int i = 0; i < 26; i++){
            if (cs[i] != 0)
                return false;
        }

        return true;
    }
}
