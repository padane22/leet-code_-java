package Q1047_removeDuplicates;

import java.util.Stack;

public class Solution {
    public String removeDuplicates(String s) {
        char[] s_c = s.toCharArray();
        Stack stack = new Stack();

        for (int i = 0; i < s.length(); i++){
            if(stack.empty()){
                stack.push(s_c[i]);
            }
            else{
                if ((char)stack.peek() == s_c[i]){
                    stack.pop();
                }
                else {
                    stack.push(s_c[i]);
                }
            }
        }

        char[] res = new char[stack.size()];
        int i = 0;
        while (!stack.empty()){
            res[i++] = (char)stack.pop();
        }

        // 字符串反转
        // 还可使用StringBuffer类
        char[] res_re = new char[res.length];
        for (i = 0; i < res.length; i++){
            res_re[res.length - 1 - i] = res[i];
        }

        return String.valueOf(res_re);
    }
}
