package Q707_linkArray;
import Structure.*;

public class MyLinkedList {
    private int size;
    private ListNode v_head;
    /** Initialize your data structure here. */
    public MyLinkedList() {
        size = 0;
        v_head = new ListNode();
    }
    /** Get the value of the index-th node in the linked list. If the index is invalid, return -1. */
    public int get(int index) {
        if (index > size - 1){
            return -1;
        }
        ListNode cur = v_head;
        for (int i = 0; i <= index; i++){
            cur = cur.next;
        }
        return cur.val;
    }

    /** Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list. */
    public void addAtHead(int val) {
        ListNode nb = new ListNode();
        nb.val = val;
        nb.next = v_head.next;
        v_head.next = nb;
        size++;
    }

    /** Append a node of value val to the last element of the linked list. */
    public void addAtTail(int val) {
        ListNode cur = v_head;
        for (int i = 0; i < size; i++){
            cur = cur.next;
        }
        ListNode nb = new ListNode();
        nb.val = val;
        cur.next = nb;
        size++;
    }

    /** Add a node of value val before the index-th node in the linked list. If index equals to the length of linked list, the node will be appended to the end of linked list. If index is greater than the length, the node will not be inserted. */
    public void addAtIndex(int index, int val) {
        ListNode cur = v_head;
        ListNode pre = new ListNode();
        for (int i = 0; i <= index; i++){
            pre = cur;
            cur = cur.next;
        }
        ListNode nb = new ListNode();
        nb.val = val;
        nb.next = cur;
        pre.next = nb;
        size++;
    }

    /** Delete the index-th node in the linked list, if the index is valid. */
    public void deleteAtIndex(int index) {
        if (index > size - 1){
            return;
        }
        ListNode cur = v_head;
        ListNode pre = new ListNode();
        for (int i = 0; i <= index; i++){
            pre = cur;
            cur = cur.next;
        }
        pre.next = cur.next;
        size--;
    }

    public static void main(String[] args) {
        MyLinkedList linkedList = new MyLinkedList();
        linkedList.addAtHead(1);
        linkedList.addAtTail(3);
        linkedList.addAtIndex(1,2);   //链表变为1-> 2-> 3
        System.out.println(linkedList.get(1));            //返回2
        linkedList.deleteAtIndex(3);  //现在链表是1-> 3
        System.out.println(linkedList.get(1));            //返回3

    }
}
