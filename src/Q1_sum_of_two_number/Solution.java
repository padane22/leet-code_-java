package Q1_sum_of_two_number;

public class Solution {
    public int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];
        int nums_len = nums.length;
        int left = 0;

        for (; left < nums_len - 1; left++){
            int right = left + 1;
            for(; right < nums_len; right++){
                if(nums[left] + nums[right] == target){
                    result[0] = left;
                    result[1] = right;

                    return result;
                }
            }
        }

        return result;
    }
}
