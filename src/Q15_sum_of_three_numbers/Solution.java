package Q15_sum_of_three_numbers;
import java.util.*;

public class Solution {
    /*
    * 执行用时：2503 ms,
    * 在所有 Java 提交中击败了5.01%的用户
     */
    public List<List<Integer>> threeSum(int[] nums) {
        int len_num = nums.length;
        List<List<Integer>> res = new ArrayList<>();

        Arrays.sort(nums);

        for (int i = 0; i < len_num - 1; i++) {
            int left = i + 1;
            int right = len_num - 1;
            while (left < right) {
                int sum_abc = 0;
                sum_abc = nums[left] + nums[right] + nums[i];
                if (sum_abc == 0) {
                    List<Integer> temp = new ArrayList<>();
                    temp.add(nums[left]);
                    temp.add(nums[right]);
                    temp.add(nums[i]);
                    if (!res.contains(temp))
                        res.add(temp);

                }
                if (sum_abc > 0) {
                    right--;
                } else {
                    left++;
                }
            }
        }
        return res;
    }

    public static void main(String[] args) {
        int[] nums = {-1,0,1,2,-1,-4,-2,-3,3,0,4};
        Solution s = new Solution();
        System.out.println(s.threeSum(nums));
    }
}
