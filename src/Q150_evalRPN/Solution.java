package Q150_evalRPN;

import java.util.Stack;

public class Solution {
    public int evalRPN(String[] tokens) {
        Stack<String> stack = new Stack<String>();
        for (int i = 0; i < tokens.length; i++){
            String temp = tokens[i];
            try{
                stack.push(Integer.toString(Integer.parseInt(temp)));
            }
            catch (Exception e){
                int b = Integer.parseInt(stack.pop());
                int a = Integer.parseInt(stack.pop());

                switch (temp){
                    case "+":
                        stack.push(Integer.toString(a + b));
                        break;
                    case "-":
                        stack.push(Integer.toString(a - b));
                        break;
                    case "*":
                        stack.push(Integer.toString(a * b));
                        break;
                    case "/":
                        stack.push(Integer.toString(a / b));
                        break;
                }
            }
        }

        return Integer.parseInt(stack.pop());
    }
}
