package Q213_rob2;

import java.util.Arrays;

public class Solution {
    public int rob2(int[] nums) {
        int[] nums1 = Arrays.copyOfRange(nums, 0, nums.length - 1);     // 包含 from 不包含 to
        int[] nums2 = Arrays.copyOfRange(nums, 1, nums.length);

        return Math.max(rob(nums1), rob(nums2));
    }

    public int rob(int[] nums) {
        if (nums.length == 1)   return nums[0];
        int[] dp = new int[nums.length];
        dp[0] = nums[0];
        dp[1] = Math.max(nums[0], nums[1]);

        for (int i = 2; i < nums.length; i++){
            dp[i] = Math.max(dp[i - 1], dp[i - 2] + nums[i]);
        }

        return dp[nums.length - 1];
    }

    public static void main(String[] args) {
        int[] n = {2,3,2};

        Solution s = new Solution();
        s.rob2(n);
    }
}
