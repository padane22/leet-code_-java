package Offer2_76_maxK;

public class Solution {
    public int findKthLargest(int[] nums, int k) {
        return quickSelect(nums, 0, nums.length - 1, nums.length - k);
    }

    public int quickSelect(int[] nums, int low, int high, int idx){
        int mid = partition(nums, low, high);
        if (mid == idx){
            return nums[mid];
        }
        else{
            if (idx < mid){
                return quickSelect(nums, low, mid - 1, idx);
            }
            else{
                return quickSelect(nums, mid + 1, high, idx);
            }
        }
    }

    public int partition(int[] nums, int low, int high){
        int pivot = nums[low];
        int idx = low + 1;

        for (int i = low; i <= high; i++){
            if (nums[i] < pivot){
                swap(nums, idx, i);
                idx++;
            }
        }
        swap(nums, low, idx - 1);
        return idx - 1;
    }

    public void swap(int[] nums, int a, int b){
        int temp = nums[a];
        nums[a] = nums[b];
        nums[b] = temp;
    }

}
