package Q654_constructMaximumBinaryTree;
import Structure.*;
import java.util.*;

public class Solution {
//    public Map<Integer, Integer> nums_map = new HashMap<>();
    public int[] nums;
    public TreeNode constructMaximumBinaryTree(int[] nums) {
        this.nums = nums;
        return help(0, nums.length - 1);
    }

    public TreeNode help(int left, int right){
        if(left >= right) return null;

        int max_val = -1, max_idx = 0;

        for (int i = left; i <= right; i++){
            if(nums[i] > max_val){
                max_val = nums[i];
                max_idx = i;
            }
        }

        TreeNode root = new TreeNode(max_val);
        root.left = help(left, max_idx - 1);
        root.right = help(max_idx + 1, right);

        return root;
    }
}
