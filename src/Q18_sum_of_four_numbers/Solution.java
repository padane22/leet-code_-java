package Q18_sum_of_four_numbers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {
    // 最简单的4循环做法
    public List<List<Integer>> fourSum(int[] nums, int target) {
        int len_num = nums.length;
        List<List<Integer>> res = new ArrayList<>();

        Arrays.sort(nums);

        for (int a = 0; a < len_num - 3; a++){
            for (int b = a + 1; b < len_num - 2; b++){
                for (int c = b + 1; c < len_num - 1; c++){
                    for (int d = c + 1; d < len_num; d++){
                        int sum_abcd = nums[a] + nums[b] + nums[c] + nums[d];
                        if (sum_abcd == target){
                            List<Integer> temp = new ArrayList<>();
                            temp.add(nums[a]);
                            temp.add(nums[b]);
                            temp.add(nums[c]);
                            temp.add(nums[d]);
                            if (!res.contains(temp))
                                res.add(temp);
                        }
                    }
                }
            }
        }

        return res;
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        int[] nums = {1,0,-1,0,-2,2};

        System.out.println(s.fourSum(nums, 0));
    }
}
