package Q20_isValid;

import java.util.Stack;

public class Solution {
    public boolean isValid(String s) {
        char[] s_c = s.toCharArray();
        Stack temp = new Stack();

        for(int i = 0; i < s.length(); i++){
            if(s_c[i] == '('){
                temp.push(')');
            }

            else if(s_c[i] == '['){
                temp.push(']');
            }

            else if(s_c[i] == '{'){
                temp.push('}');
            }

            else{
                if(temp.empty())
                    return false;
                else{
                    if ((char) temp.pop() != s_c[i]) {
                        return false;
                    }
                }
            }
        }


        return temp.empty() ? true: false;
    }
}
