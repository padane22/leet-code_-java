package Offer_37;

import java.util.LinkedList;
import java.util.Queue;

public class Solution {

    public static void main(String[] args) {
        Solution s = new Solution();
        s.deserialize2("[]");
    }

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        Queue<TreeNode> roots = new LinkedList<TreeNode>();
        roots.offer(root);
        String res = "[";

        if(root == null){
            return "[]";
        }
        while(!roots.isEmpty()){
            TreeNode temp = roots.poll();
            if(temp != null){
                res += Integer.toString(temp.val);
                res += ",";
                roots.offer(temp.left);
                roots.offer(temp.right);
            }
            else {
                res += "null,";
            }
        }
        return res.substring(0, res.length()-1) + "]";

    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if(data.equals("[]"))
            return null;
        data = data.substring(1, data.length() - 1);
        String[] datas = data.split(",");
        TreeNode root = new TreeNode(Integer.parseInt(datas[0]));
        Queue<TreeNode> roots = new LinkedList<TreeNode>();
        roots.offer(root);
        int i = 1;
        for(; i < datas.length; i++){
            TreeNode temp = roots.poll();

            try {
                temp.left = new TreeNode(Integer.parseInt(datas[i]));
                roots.offer(temp.left);
            }
            catch (NumberFormatException e){

            }

            i++;
            if(i == datas.length)
                break;

            try {
                temp.right = new TreeNode(Integer.parseInt(datas[i]));
                roots.offer(temp.right);
            }
            catch (NumberFormatException e){

            }

        }

        return root;
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize2(String data) {
        if(data.equals("[]")){
            return null;
        }
        data = data.substring(1, data.length() - 1);
        String[] datas = data.split(",");
        Queue<TreeNode> roots = new LinkedList();
        TreeNode root = new TreeNode(Integer.parseInt(datas[0]));

        roots.add(root);

        for (int i = 1; i < datas.length; i++){
            TreeNode temp = roots.poll();
            int val = 0;

            if(!datas[i].equals("null")){
                val = Integer.parseInt(datas[i]);
                temp.left = new TreeNode(val);
                roots.add(temp.left);
            }


            i++;
            if(i >= datas.length){
                break;
            }
            if(!datas[i].equals("null")){
                val = Integer.parseInt(datas[i]);
                temp.right = new TreeNode(val);
                roots.add(temp.right);
            }


        }
        return root;
    }
}
