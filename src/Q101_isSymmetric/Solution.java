package Q101_isSymmetric;
import Structure.*;

public class Solution {
    public boolean isSymmetric(TreeNode root) {
        if (root == null)
            return true;
        if (root.left == null && root.right == null)
            return true;
        if (root.left == null || root.right == null)
            return false;

        if (root.left.val == root.right.val)
            return isSame(root.left, root.right);
        else
            return false;
    }

    public boolean isSame(TreeNode a, TreeNode b){

        if (a == null && b == null)
            return true;
        if (a == null || b == null)
            return false;

        return a.val == b.val && isSame(a.left, b.right) && isSame(a.right, b.left);
    }
}
