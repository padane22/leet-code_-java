package Q106_buildTree;
import Structure.*;

import java.util.HashMap;
import java.util.Map;

public class Solution {
    /**
     * 根据中序和后序遍历构造二叉树
     * @param inorder
     * @param postorder
     * @return
     */
    public int[] inorder, postorder;
    // 存放后序遍历的当前下标
    public int post_idx;
    // 对中序遍历的数组建立 hash 映射，回忆查找速度；只能用于值不重复的情况
    public Map<Integer, Integer> inorder_map = new HashMap<>();

    public TreeNode buildTree(int[] inorder, int[] postorder) {
        this.inorder = inorder;
        this.postorder = postorder;
        post_idx = postorder.length - 1;

        for (int i = 0; i < inorder.length; i++){
            inorder_map.put(inorder[i], i);
        }

        return help(0, inorder.length - 1);

    }

    public TreeNode help(int in_left, int in_right){
        if (in_left > in_right)
            return null;

        TreeNode root = new TreeNode(postorder[post_idx]);

        int in_idx = inorder_map.get(root.val);

        post_idx--;

        // 前序和中序时 要先构造左子树 再构造右子树；而后序和中序时 要先构造右子树 再构造左子树
        root.right = help(in_idx + 1, in_right);
        root.left = help(in_left, in_idx - 1);

        return root;
    }
}
