package Offer_07_buildTree;
import Structure.*;

import java.util.HashMap;
import java.util.Map;

public class Solution {
    public int[] inorder, preorder;
    public int pre_idx;
    // 对中序遍历的数组建立 hash 映射，回忆查找速度；只能用于值不重复的情况
    public Map<Integer, Integer> inorder_map = new HashMap<>();

    public TreeNode buildTree(int[] preorder, int[] inorder) {
        this.inorder = inorder;
        this.preorder = preorder;
        pre_idx = 0;

        for (int i = 0; i < inorder.length; i++){
            inorder_map.put(inorder[i], i);
        }

        return help(0, inorder.length - 1);

    }

    public TreeNode help(int in_left, int in_right){
        if (in_left > in_right)
            return null;

        TreeNode root = new TreeNode(preorder[pre_idx]);

        int in_idx = inorder_map.get(root.val);

        pre_idx++;

        // 如果这两行代码交换则会出错？
        // 前序和中序时 要先构造左子树 再构造右子树；而后序和中序时 要先构造右子树 再构造左子树
        root.left = help(in_left, in_idx - 1);
        root.right = help(in_idx + 1, in_right);

        return root;
    }

}
